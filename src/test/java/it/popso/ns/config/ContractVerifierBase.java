package it.popso.ns.config;

import io.restassured.RestAssured;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import it.popso.ns.GestoreAllegatiArchetipoApplication;


@ExtendWith(SpringExtension.class)
@SpringBootTest(classes = GestoreAllegatiArchetipoApplication.class, webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class ContractVerifierBase {



    @LocalServerPort
    int port;

    @BeforeEach
    public void setUp() throws Exception {
        RestAssured.baseURI = "http://localhost";
        RestAssured.port = this.port;
    }
}