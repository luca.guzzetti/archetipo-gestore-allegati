package it.popso.ns.config;

import lombok.RequiredArgsConstructor;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;
import it.popso.ns.entities.DummyEntity;
import it.popso.ns.repositories.DummyRepository;


@Component
@RequiredArgsConstructor
public class Bootstrap implements CommandLineRunner{

    private final DummyRepository dummyRepository;

    @Override
    public void run(String... args) throws Exception {
        dummyRepository.save(DummyEntity.builder()
                .value("value-1")
                .build()
        );
    }
}