package it.popso.ns.services;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import it.popso.ns.repositories.DummyRepository;
import it.popso.ns.entities.DummyEntity;
import it.popso.ns.exceptions.ApplicationFault;

@Service
@RequiredArgsConstructor
public class DummyService {

    private final DummyRepository dummyRepository;

    public DummyEntity getDummyEntityById(Integer id) {
        return dummyRepository.findById(id).orElseThrow(() -> new ApplicationFault("Entity Not Found id:" + id));
    }
}