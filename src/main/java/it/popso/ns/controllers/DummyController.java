package it.popso.ns.controllers;

import io.github.resilience4j.circuitbreaker.CallNotPermittedException;
import lombok.RequiredArgsConstructor;
import io.github.resilience4j.circuitbreaker.annotation.CircuitBreaker;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import java.util.concurrent.atomic.AtomicBoolean;
import org.springframework.util.MultiValueMap;
import it.popso.ns.dto.Fault;
import it.popso.ns.services.DummyService;
import it.popso.ns.exceptions.*;

@RestController
@RequiredArgsConstructor
public class DummyController extends ControllerBase {

    private final DummyService dummyService;

    @GetMapping("/v1/dummy/{id}")
    @CircuitBreaker(name = "dummy", fallbackMethod = "fallbackMethod")
    ResponseEntity<Object> getDummy(@PathVariable Integer id, @RequestHeader MultiValueMap<String, String> headers) {

        HttpHeaders responseHeaders = new HttpHeaders();
        responseHeaders.set("X-Bps-Tt-IdOperazione-CodiceApplicazioneChiamante", "test");
        responseHeaders.set("X-Bps-Tt-IdOperazione-CodiceOperazione", "test");
        responseHeaders.set("X-Bps-Tt-IdConversazione", "OK");

        AtomicBoolean ok = new AtomicBoolean(false);


        headers.forEach((k, v) -> {
            if (k.equalsIgnoreCase("result")) {
                ok.set(true);
            }
        });

        if (ok.get()) {
            return ResponseEntity.ok()
                    .headers(responseHeaders)
                    .body(dummyService.getDummyEntityById(id));
        }
        return ResponseEntity.ok()
                .headers(responseHeaders)
                .body(dummyService.getDummyEntityById(0));
    }

    ResponseEntity<Object> fallbackMethod(Integer id, @RequestHeader MultiValueMap<String, String> headers, RuntimeException ex) throws ApplicationFault, DatiTestataFault, DubbiaNonRisottomissibileFault,
            DubbiaRisottomissibileConInterventoManualeFault, DubbiaRisottomissibileFault, InputFault,
            ServizioNonDisponibileFault, SystemFault {

        try {
            throw ex;
        } catch (CallNotPermittedException ex1) {
            throw new ServizioNonDisponibileFault("Service Unavailable - CircuitBreaker Pattern Applied");
        }


    }

    @Override
    ResponseEntity<Object> getFaultResponse(HttpStatus status, RuntimeException ex) {

        HttpHeaders responseHeaders = new HttpHeaders();
        responseHeaders.set("Content-Type", "application/json");
        responseHeaders.set("X-Bps-Tt-IdOperazione-CodiceApplicazioneChiamante", "test");
        responseHeaders.set("X-Bps-Tt-IdOperazione-CodiceOperazione", "test");
        responseHeaders.set("X-Bps-Tt-IdConversazione", "OK");

        Fault.ClasseFault classeFault = null;
        String codice = null;
        String messaggio = null;
        String layer = null;

        if (ex.getClass() == ApplicationFault.class) {
            classeFault = Fault.ClasseFault.APPLICATION_FAULT;
            codice = "400";
            messaggio = ex.getMessage();
            layer = "test";
        } else if (ex.getClass() == DatiTestataFault.class) {
            classeFault = Fault.ClasseFault.DATI_TESTATA_FAULT;
            codice = "400";
            messaggio = ex.getMessage();
            layer = "test";
        } else if (ex.getClass() == SystemFault.class) {
            classeFault = Fault.ClasseFault.SYSTEM_FAULT;
            codice = "500";
            messaggio = ex.getMessage();
            layer = "test";
        } else if (ex.getClass() == ServizioNonDisponibileFault.class) {
            classeFault = Fault.ClasseFault.SERVIZIO_NON_DISPONIBILE_FAULT;
            codice = "500";
            messaggio = ex.getMessage();
            layer = "test";
        } else if (ex.getClass() == InputFault.class) {
            classeFault = Fault.ClasseFault.INPUT_FAULT;
            codice = "400";
            messaggio = ex.getMessage();
            layer = "test";
        } else if (ex.getClass() == DubbiaRisottomissibileFault.class) {
            classeFault = Fault.ClasseFault.DUBBIA_RISOTTOMISSIBILE_FAULT;
            codice = "500";
            messaggio = ex.getMessage();
            layer = "test";
        } else if (ex.getClass() == DubbiaNonRisottomissibileFault.class) {
            classeFault = Fault.ClasseFault.DUBBIA_NON_RISOTTOMISSIBILE_FAULT;
            codice = "500";
            messaggio = ex.getMessage();
            layer = "test";
        } else if (ex.getClass() == DubbiaRisottomissibileConInterventoManualeFault.class) {
            classeFault = Fault.ClasseFault.DUBBIA_NON_RISOTTOMISSIBILE_FAULT;
            codice = "500";
            messaggio = ex.getMessage();
            layer = "test";
        }


        return ResponseEntity.status(status)
                .headers(responseHeaders)
                .body(
                        getFaultObject(classeFault, codice, messaggio, layer)
                );
    }
}