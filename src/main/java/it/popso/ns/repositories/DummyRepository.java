package it.popso.ns.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import it.popso.ns.entities.DummyEntity;

public interface DummyRepository extends JpaRepository<DummyEntity, Integer> {

}