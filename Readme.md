# Spring Boot Cookbook
>Questo progetto è stato generato automaticamente con il tool **rest-springboot-archetype**.

>L'obiettivo del seguente cookbook è quello di semplicare e standardizzare le metodologie e gli strumenti richiesta da banca per l'integrazione nel **SDLC** degli applicativi spring boot.  
L'artefatto viene fornito con un controller sonda di test  debuggabile, che intrega tutte le funzionalità offerte dal cookbook.

>Il presente cookbook è compliant con gli standard OpenApi v3 e con le **ultime linee guida banca** per quanto riguarda l'integrazione in un contesto multicanale.  

>Per agevolare il processo di code coverage il presente cookbook viene fornito con l'integrazione con [Spring Cloud Contract](https://spring.io/projects/spring-cloud-contract) facoltativo (per non utilizzarlo basta rimuovere il plugin di maven dal pom), che permette in modo dichiarativo attraverso un DSL di definire i casi d'uso da testare, sarà il pluglin di maven integrato a generare a compile time i test e produrre degli stub compatibili con wiremock pacchettizati in un jar separato deployabile su nexus aziendale, in ogni caso per maggiori informazioni si può fare riferimento alla documentazione ufficiale linkata prima.  
Per la defizione dei contratti utili a spring cloud contract e stata altresi adottata un'estenzione che permette la defizione degli stessi attraverso la specifica openapi, il controller di sample implementa due esempi di contratto che utilizzando la suddetta libreia, in ogni caso di seguito il riferimento alla documentazione ufficiale di seguito linkata:  
[Spring Cloud Contract OpenAPI 3.0 Contract Converter](https://github.com/springframeworkguru/spring-cloud-contract-oa3)

>Il cookbook implementa il pattern architetturale del **Circuit Breaker** attraverso l'adozione della libreia res4j.  
Nel Controller di sample vi è un'implementazione concreta su come si dovrebbe gestire il fallback quando il circuito si trova nello stato aperto.  
Per la configurazione delle properties si rimanda alla documentazione ufficiale di [res4j](https://resilience4j.readme.io/docs/getting-started-3).

>Il cookbook viene fornito con un controller base che gestisce automaticamente il mapping tra stus code e codice di errore banca:  
```
 public abstract class ControllerBase {

    abstract ResponseEntity<Object> getFaultResponse(HttpStatus status, RuntimeException ex);

    protected Fault getFaultObject(Fault.ClasseFault classeFault, String codice, String messaggio, String layer) {
        return Fault.builder()
                .classeFault(classeFault)
                .codice(codice)
                .messaggio(messaggio)
                .layer(layer)
                .build();
    }


    @ExceptionHandler
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    ResponseEntity<Object> applicationFaultException(ApplicationFault e) {

        return getFaultResponse(HttpStatus.BAD_REQUEST, e);
    }

    @ExceptionHandler
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    ResponseEntity<Object> datiTestataFaultException(DatiTestataFault e) {

        return getFaultResponse(HttpStatus.BAD_REQUEST, e);
    }

    @ExceptionHandler
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    ResponseEntity<Object> inputFaultException(InputFault e) {

        return getFaultResponse(HttpStatus.BAD_REQUEST, e);
    }

    @ExceptionHandler
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    ResponseEntity<Object> servizioNonDisponibileFaultException(ServizioNonDisponibileFault e) {

        return getFaultResponse(HttpStatus.INTERNAL_SERVER_ERROR, e);
    }

    @ExceptionHandler
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    ResponseEntity<Object> systemFaultException(SystemFault e) {

        return getFaultResponse(HttpStatus.INTERNAL_SERVER_ERROR, e);
    }

    @ExceptionHandler
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    ResponseEntity<Object> dubbiaRisottomissibileFaultException(DubbiaRisottomissibileFault e) {

        return getFaultResponse(HttpStatus.INTERNAL_SERVER_ERROR, e);
    }

    @ExceptionHandler
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    ResponseEntity<Object> dubbiaNonRisottomissibileFaultException(DubbiaNonRisottomissibileFault e) {

        return getFaultResponse(HttpStatus.INTERNAL_SERVER_ERROR, e);
    }

    @ExceptionHandler
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    ResponseEntity<Object> dubbiaRisottomissibileConInterventoManualeFaultException(DubbiaRisottomissibileConInterventoManualeFault e) {

        return getFaultResponse(HttpStatus.INTERNAL_SERVER_ERROR, e);
    }
}
```
